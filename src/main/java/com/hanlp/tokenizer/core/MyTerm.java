package com.hanlp.tokenizer.core;

import com.hankcs.hanlp.corpus.tag.Nature;
import com.hankcs.hanlp.seg.common.Term;

import java.util.Objects;

public class MyTerm extends Term {
    public MyTerm(String word, Nature nature) {
        super(word, nature);
    }

    public MyTerm(Term term) {
        super(term.word, term.nature);
        this.offset = term.offset;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MyTerm) {
            MyTerm term = (MyTerm) obj;
            if (Objects.equals(this.offset, term.offset) && this.word.equals(term.word)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.word.hashCode() + this.offset;
    }
}
